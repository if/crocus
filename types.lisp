;;; types.lisp
;;; a typechecker for crocus
(in-package #:crocus)

;;; overview:
;;; - types are represented using cons cells
;;; - return type of a function is listed first
;;;   - e.g. '(:fn :bool :num :num) ≡ num -> num -> bool
;;; - possible types:
;;;   - basic (:bool, :num)
;;;   - functions


;; TODO: it's good to have this as a parameter when
;; it comes to repl interaction, but eventually it's
;; probably going to be necessary to make the type
;; environment local instead
(defparameter *type-env* (make-empty-environment))
;; variable store for types
(defparameter *type-store* (init-var-store))


(defun type-builtin (builtin)
  "get the type of a built-in function/variable"
  (cond ((string-member builtin '("+" "-" "*" "/"))
         '(:fn :num :num :num))
        ((string-member builtin '(">" "<" "="))
         '(:fn :bool :num :num))
        ((string= builtin "not") '(:fn :bool :bool))
        ((string= builtin "and") '(:fn :bool :bool :bool))
        ((string= builtin  "or") '(:fn :bool :bool :bool))
        ((string= builtin "negate") '(:fn :num :num))
        ((string= builtin "cat") '(:fn :str :str :str))
        ((string= builtin "print") '(:fn :void :str))
        (t nil)))


(defun fresh-variable ()
  "create a new unification variable"
  (intern (format nil "?~s" (gensym))))

(defun type-variable (sym)
  "get the type of a variable"
  (or (type-builtin sym)
      (lookup-var *type-store* sym)))

(defun type-apply (fn args)
  "typecheck a function application"
  (let* ((ret (fresh-variable))
         (fn-type (typecheck fn))
         (derived-fn-type
           (concatenate 'list '(:fn) (list ret) (mapcar #'typecheck args))))
    (debug-log "~a ~~ ~a~%" fn-type derived-fn-type)
    (unify fn-type derived-fn-type *type-env*)
    (find-variable-value ret *type-env*)))

(defun type-let (bindings body)
  "typecheck a let-binding environment"
  ;; push scope and typecheck bindings
  (+depth *type-store*)
  (mapcar
   (lambda (b) (let ((name (car b))
                     (type (typecheck (cdr b))))
                 (store-var *type-store* name type)))
   bindings)
  (let* ((type (typecheck body)))
    ;; decrement depth before returning
    (-depth *type-store*)
    type))

;; TODO: will have to figure out how to type non-values
;; when polymorphism comes into play; i.e. prevent
;; (id (var foo 2)) : `:no'
(defun type-global (name val)
  "typecheck a global variable declaration"
  (let ((type (typecheck val)))
    (store-var *type-store* name type)
    (debug-log "~a => ~a~%" name type)
    :no))

(defun type-if (val)
  "typecheck an `if'-conditional form"
  (let* ((pred (car val))
         (true-path (cadr val))
         (false-path (cddr val))
         (tpred (typecheck pred))
         (ttrue (typecheck true-path))
         (tfalse (typecheck false-path)))
    (unify tpred :bool *type-env*)
    (unify ttrue tfalse *type-env*)
    ttrue))

(defun type-lambda (param body)
  "typecheck a lambda function"
  ;; 1. bind parameter to unification variable
  ;; 2. push depth and store parameter as local
  ;; 3. typecheck lambda's body
  ;; 4. extract parameter's new type
  ;; 5. pop depth and build function type
  (let ((v (fresh-variable)))
    (+depth *type-store*)
    (store-var *type-store* param v)
    (let* ((ret (typecheck body))
           (v* (or (find-variable-value v *type-env*) v))
           (fun (list :fn ret v*)))
      (-depth *type-store*)
      fun)))

(defun type-tuple (tuple-elems)
  (if tuple-elems
      (let ((types (mapcar #'typecheck tuple-elems)))
        (cons :tuple types))
      (list :tuple)))

(defun typecheck (expr)
  (let ((flavor (car expr))
        (value (cdr expr)))
    (case flavor
      (:apply (type-apply (car value) (cdr value)))
      (:sym (type-variable value))
      (:var-decl (type-global (car value) (cdr value)))
      (:let (type-let (car value) (cdr value)))
      (:if (type-if value))
      (:lambda (type-lambda (car value) (cdr value)))
      (:tuple (type-tuple value))
      (:str :str)
      (:crunch :crunch)
      (:number :num)
      (:bool :bool)
      (t (error (format nil "can't typecheck `~a'~%" expr))))))
