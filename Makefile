CC=gcc
CFLAGS=-g -Wall -Wformat=0
CSOURCES=*.c


clean:
	rm -vrf crocus.so test crocus

test: $(CSOURCES)
	$(CC) -o $@ $(CFLAGS) $^

crocus.so: *.c
	$(CC) -shared -o $@ $(CFLAGS) -fPIC $^

crocus: *.lisp crocus.so
	sbcl --non-interactive --load build.lisp
