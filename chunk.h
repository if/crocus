#ifndef __CHUNK_H__
#define __CHUNK_H__

#include "common.h"
#include "value.h"

typedef enum {
    OP_RETURN,
    /* consts */
    OP_CONSTANT,
    OP_TRUE,
    OP_FALSE,
    OP_NIL,
    /* operators */
    OP_NEGATE,
    OP_ADD,
    OP_SUBTRACT,
    OP_MULTIPLY,
    OP_DIVIDE,
    OP_NOT,
    OP_AND,
    OP_OR,
    OP_EQ,
    OP_GT,
    OP_LT,
    OP_CAT,
    OP_PRINT,
    OP_POP,
    /* global variables */
    OP_DGLOBAL,
    OP_LGLOBAL,
    /* local variables */
    OP_DLOCAL,
    OP_LLOCAL,
    /* jumping */
    OP_JUMP,
    OP_JUMPZ, /* 'jump if zero' */
    /* function stuff */
    OP_CALL,
    OP_CLOSURE,
    OP_GET_UPVALUE,
    OP_SET_UPVALUE,
    OP_CLOSE_UPVALUE,
    /* tuple get/set */
    OP_TUP_GET,
    OP_TUP_SET,
} opcode;

typedef struct {
    int count;
    int capacity;
    uint8_t *code;
    int *lines;
    value_array consts;
} chunk;


chunk *make_chunk();
void delete_chunk(chunk *chunk);

void init_chunk(chunk *chunk);
void free_chunk(chunk *chunk);
void write_chunk(chunk *chunk, uint8_t byte, int line);
void overwrite_chunk(chunk *chunk, uint8_t byte, int offset);
int add_const(chunk *chunk, value val);
int add_const_compat(chunk *chunk, value *val);

#endif
