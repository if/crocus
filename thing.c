#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "memory.h"
#include "thing.h"
#include "value.h"
#include "tab.h"
#include "vm.h"

#define ALLOC_THING(type, thing_type)           \
  (type*)alloc_thing(sizeof(type), thing_type)




void print_thing(value val) {
  switch (THING_TYPE(val)) {
  case THING_STR: {
    printf("%s", AS_CSTR(val));
    break;
  }

  case THING_FUN: {
    thing_fun *fun = AS_FUN(val);
    printf("<λ.%x.%x>", fun->arity, &fun->chunk);
    break;
  }

  case THING_CLOSURE: {
    thing_fun* fun = AS_CLOSURE(val)->fun;
    printf("<λ[..].%x.%x>", fun->arity, &fun->chunk);
    break;
  }

  case THING_UPVAL:  {
    /* upvals won't really get printed so this
       is a placeholder really */
    printf("upvalue");
    break;
  }

  case THING_TUP: {
    thing_tup *tup = AS_TUP(val);

    printf("<");
    for (int i = 0; i < tup->len; i++) {
      if (i != 0) printf(" ");
      print_value(tup->vals[i]);
    }
    printf(">");

    break;
  }
  }
}


static thing *alloc_thing(size_t size, thing_type type) {
  thing *thing = reallocate(NULL, 0, size);
  thing->type = type;

  /* save self in vm's linked list of things */
  thing->next = vm.things;
  vm.things = thing;
    
  return thing;
}

static thing_str *alloc_str(char *chars, int len, uint32_t hash) {
  thing_str *str = ALLOC_THING(thing_str, THING_STR);
  str->len = len;
  str->chars = chars;
  str->hash = hash;
  /* "intern" the string as a key with a dummy value */
  table_set(&vm.strings, str, NIL_VAL);
    
  return str;
}

/* the FNV-1a hash function (see "Crafting Interpreters", ch 20.3) */
static uint32_t hash_str(const char *key, int len) {
  uint32_t hash = 2166136261u;

  for (int i = 0; i < len; i++) {
    hash ^= (uint8_t)key[i];
    hash *= 16777619;
  }

  return hash;
}

thing_str *take_str(char *chars, int len) {
  uint32_t hash = hash_str(chars, len);
  
  /* if the string is already interned, use it instead */
  thing_str *interned = table_find_str(&vm.strings, chars, len, hash);
  if (interned != NULL) {
    /* only free if the string has a different address than the
       interned string; sometimes it ends up with the same address
       as the interned string, which would result in a double free */
    if (chars != interned->chars)
      FREE_ARRAY(char, chars, len+1);
    return interned;
  }

  return alloc_str(chars, len, hash);
}


thing_fun *new_fun() {
  thing_fun *fun = ALLOC_THING(thing_fun, THING_FUN);
  fun->arity = 1;               /* TODO: parameterize */
  init_chunk(&fun->chunk);
  fun->num_upvalues = 0;
  return fun;
}

/* a little silly but it makes ffi'ing easier trust me */
chunk *chunk_from_fun(thing_fun *fun) {
  return &fun->chunk;
}


/* make an empty tuple of fixed length `len' */
thing_tup *make_tuple(int len) {
  thing_tup *tup = ALLOC_THING(thing_tup, THING_TUP);
  tup->len = len;
  tup->vals = calloc(len, sizeof(value));
  return tup;
}


/* convenient for lisp interop */
value *make_tuple_value(int len) {
  return make_thing_value((thing*)make_tuple(len));
}




thing_closure *new_closure(thing_fun *fun) {
  thing_closure *closure = ALLOC_THING(thing_closure, THING_CLOSURE);
  closure->fun = fun;

  thing_upval **upvals = ALLOC(thing_upval*, fun->num_upvalues);
  for (int i = 0; i < fun->num_upvalues; i++)
    upvals[i] = NULL;

  closure->upvals = upvals;
  closure->num_upvals = fun->num_upvalues;

  return closure;
}


thing_upval *new_upval(value *slot) {
  thing_upval *uv = ALLOC_THING(thing_upval, THING_UPVAL);
  uv->location = slot;
  uv->saved = NULL;
  return uv;
}
