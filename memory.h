#ifndef __MEMORY_H__
#define __MEMORY_H__

#include "common.h"
#include "thing.h"


#define ALLOC(type, count) \
    (type*)reallocate(NULL, 0, sizeof(type) * (count))

#define GROW_CAPACITY(capacity) \
    ((capacity < 8)? 8 : (capacity) * 2)

#define GROW_ARRAY(ty, ptr, oldc, newc) \
    (ty*)reallocate(ptr, sizeof(ty) * (oldc), \
                    sizeof(ty) * (newc))

#define FREE(type, ptr) reallocate(ptr, sizeof(type), 0)

#define FREE_ARRAY(ty, ptr, oldc) \
    reallocate(ptr, sizeof(ty) * (oldc), 0)


void *reallocate(void *ptr, size_t old_size, size_t new_size);
void free_things();

#endif
