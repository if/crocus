#include <stdio.h>
#include "crocus.h"
#include "thing.h"

/** 
    This file has a `main' function and is just meant for testing programs directly in C,
    rather than through an FFI or by reading code from a source file.
*/


int run_chunk(chunk *chunk) {
  puts("");
  
  // init_vm();
  /* put `chunk' inside a function */
  thing_fun *entry = new_fun();
  entry->chunk = *chunk;
  interpret(entry);
  /* print top of stack */
  printf("=> ");
  print_value(peek(1));
  printf("\n");
  /* reset stack (may want to remove later) */
  reset_stack();

  // TODO: the VM must be freed elsewhere;
  // figure this out when you start working
  // on the REPL.
  // free_vm();
  free_chunk(chunk);

  return 0;
}


int main() {
  init_vm();

  chunk *c = make_chunk();
  init_chunk(c);

  write_chunk(c, OP_CONSTANT, 0);
  write_chunk(c, add_const_compat(c, make_tuple_value(2)), 0);
  write_chunk(c, OP_CONSTANT, 0);
  write_chunk(c, add_const_compat(c, make_bool_value(true)), 0);
  write_chunk(c, OP_TUP_SET, 0);
  write_chunk(c, 0, 0);
  write_chunk(c, OP_CONSTANT, 0);
  write_chunk(c, add_const_compat(c, make_str_value("#cofe", 5)), 0);
  write_chunk(c, OP_TUP_SET, 0);
  write_chunk(c, 1, 0);
  write_chunk(c, OP_TUP_GET, 0);
  write_chunk(c, 1, 0);

  disassemble_chunk(c, "...");
  puts("");

  run_chunk(c);
  
  return 0;
}
