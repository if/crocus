#ifndef __VALUE_H__
#define __VALUE_H__
#include "common.h"

typedef struct thing thing;
typedef struct thing_str thing_str;
typedef struct thing_fun thing_fun;
typedef struct thing_closure thing_closure;
typedef struct thing_upval thing_upval;
typedef struct thing_tup thing_tup;


/* type primitive 'type' of a value */
typedef enum {
    VALUE_NUM,
    VALUE_NIL,
    VALUE_BOOL,
    /* a heap-allocated value */
    VALUE_THING,
} value_type;

/* the 'value' of a value */
typedef union {
    bool boolean;
    double num;
    thing *thing;
} value_form;

/* contains type and value information */
typedef struct {
    value_type type;
    value_form as;
} value;

/* holds many values */
typedef struct {
    int capacity;
    int count;
    value *values;
} value_array;



#define BOOL_VAL(val) ((value){VALUE_BOOL, {.boolean = val}})
#define  NIL_VAL      ((value){VALUE_NIL, {.num = 0}})
#define  NUM_VAL(val) ((value){VALUE_NUM, {.num = val}})
#define THING_VAL(val) ((value){VALUE_THING, {.thing = (thing*)val}})
#define AS_BOOL(val) ((val).as.boolean)
#define  AS_NUM(val) ((val).as.num)
#define AS_THING(val) ((val).as.thing)
#define IS_BOOL(val) ((val).type == VALUE_BOOL)
#define  IS_NUM(val) ((val).type == VALUE_NUM)
#define  IS_NIL(val) ((val).type == VALUE_NIL)
#define IS_THING(val) ((val).type == VALUE_THING)
value_array *make_value_array(int cap, int count, value *vals);

void init_value_array(value_array *arr);
void free_value_array(value_array *arr);
void write_value_array(value_array *arr, value val);
void print_value(value val);
void print_value_compat(value *val);
value value_at(value_array arr, uint8_t index);
bool values_equal(value a, value b);

value *make_bool_value(bool b);
value *make_nil_value();
value *make_num_value(double num);
value *make_str_value(char *chars, int len);
value *make_thing_value(thing *th);

#endif
