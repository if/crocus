#ifndef __CROCUS_H__
#define __CROCUS_H__

#include "common.h"
#include "chunk.h"
#include "debug.h"
#include "vm.h"

int run_chunk(chunk *chunk);

#endif
