#include <stdio.h>
#include "debug.h"
#include "value.h"
#include "thing.h"


static int simple_instr(const char *name, int offset) {
  printf("%s\n", name);
  return offset + 1;
}

static int const_instr(const char *name, chunk *chunk,
                       int offset) {
  uint8_t constant = chunk->code[offset + 1];
  printf("%-16s %4d '", name, constant);
  print_value(value_at(chunk->consts, constant));
  printf("'\n");

  return offset + 2;
}

static int byte_instr(const char *name, chunk *chunk,
                      int offset) {
  uint8_t slot = chunk->code[offset+1];
  printf("%-16s %4d\n", name, slot);
  return offset + 2;
}

static int jump_instr(const char *name, int sign,
                      chunk *chunk, int offset) {
  uint16_t jump = (uint16_t)(chunk->code[offset+1] << 8);
  jump |= chunk->code[offset+2];

  printf("%-16s %4d -> %d\n", name, offset,
         offset + 3 + (sign * jump));

  return offset+3;
}



void disassemble_chunk(chunk *chunk, const char *name) {
  printf("== %s ==\n", name);

  for (int offset = 0; offset < chunk->count;)
    offset = disassemble_instr(chunk, offset);

  puts("****");
}

int disassemble_instr(chunk *chunk, int offset) {
  printf("%04d ", offset);
    
  if (offset > 0 &&
      chunk->lines[offset - 1] == chunk->lines[offset]) {
    printf("   ^ ");
  } else {
    printf("%04d ", chunk->lines[offset]);
  }

    
  uint8_t instr = chunk->code[offset];
  switch (instr) {
  case OP_RETURN:
    return simple_instr("OP_RETURN", offset);
  case OP_PRINT:
    return simple_instr("OP_PRINT", offset);
  case OP_POP:
    return simple_instr("OP_POP", offset);
  case OP_DGLOBAL:
    return const_instr("OP_DGLOBAL", chunk, offset);
  case OP_LGLOBAL:
    return const_instr("OP_LGLOBAL", chunk, offset);
  case OP_DLOCAL:
    return byte_instr("OP_DLOCAL", chunk, offset);
  case OP_LLOCAL:
    return byte_instr("OP_LLOCAL", chunk, offset);
  case OP_CONSTANT:
    return const_instr("OP_CONSTANT", chunk, offset);
  case OP_NEGATE:
    return simple_instr("OP_NEGATE", offset);
  case OP_ADD:
    return simple_instr("OP_ADD", offset);
  case OP_SUBTRACT:
    return simple_instr("OP_SUBTRACT", offset);
  case OP_MULTIPLY:
    return simple_instr("OP_MULTIPLY", offset);
  case OP_DIVIDE:
    return simple_instr("OP_DIVIDE", offset);
  case OP_TRUE:
    return simple_instr("OP_TRUE", offset);
  case OP_FALSE:
    return simple_instr("OP_FALSE", offset);
  case OP_NIL:
    return simple_instr("OP_NIL", offset);
  case OP_NOT:
    return simple_instr("OP_NOT", offset);
  case OP_AND:
    return simple_instr("OP_AND", offset);
  case OP_OR:
    return simple_instr("OP_OR", offset);
  case OP_EQ:
    return simple_instr("OP_EQ", offset);
  case  OP_GT:
    return simple_instr("OP_GT", offset);
  case OP_LT:
    return simple_instr("OP_LT", offset);
  case OP_CAT:
    return simple_instr("OP_CAT", offset);
  case OP_JUMP:
    return jump_instr("OP_JUMP", 1, chunk, offset);
  case OP_JUMPZ:
    return jump_instr("OP_JUMPZ", 1, chunk, offset);
  case OP_CALL:
    return byte_instr("OP_CALL", chunk, offset);
  case OP_TUP_GET:
    return byte_instr("OP_TUP_GET", chunk, offset);
  case OP_TUP_SET:
    return byte_instr("OP_TUP_SET", chunk, offset);
  case OP_CLOSURE: {
    offset++;
    uint8_t constant = chunk->code[offset++];
    printf("%-16s %4d ", "OP_CLOSURE", constant);
    print_value(chunk->consts.values[constant]);
    puts("");

    thing_fun *fun = AS_FUN(chunk->consts.values[constant]);
    for (int j = 0; j < fun->num_upvalues; j++) {
      int localp = chunk->code[offset++];
      int index = chunk->code[offset++];
      printf("%04d    |        %s %d\n",
             offset-2, localp? "local" : "upvalue", index);
    }
    
    return offset;
  }

  case OP_GET_UPVALUE:
    return byte_instr("OP_GET_UPVALUE", chunk, offset);
  case OP_SET_UPVALUE:
    return byte_instr("OP_SET_UPVALUE", chunk, offset);

  default:
    printf("unknown opcode %d\n", instr);
    return offset + 1;
  }
}
