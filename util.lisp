;;; util.lisp
;;; general utilies

(in-package #:crocus)

(defun string-member (str list)
  (reduce #'(lambda (p s) (or p (string= s str)))
          list :initial-value nil))

(defun intersperse (y xs)
  "put `y' in between the elements of the list `xs'"
  (if (cdr xs)
      (cons (car xs) (cons y (intersperse y (cdr xs))))
      xs))

(defun filter (p xs)
  "retain elements of `xs' which satisfy predicate `p'"
  (loop for x in xs
        if (funcall p x) collect x))


;; TODO: figure out a way to define builtins once, then
;; use them across compiler modules (typechecker and bytecode output)
(defun builtinp (name)
  "determine whether `name' names a built-in function"
  (string-member name '("+" "-" "*" "/" ">" "<" "="
                        "not" "negate" "print" "cat" "and" "or")))


(defun dot (xs)
  "transform a list into a dotted list"
  (apply #'list* xs))


(defun split-while (p xs)
  "separate start of `xs' that satisfies `p';\
return in form (satisfies p . rest)"
  (if (funcall p (car xs))
      (let* ((result (split-while p (cdr xs)))
             (pass (car result))
             (rest (cdr result)))
        (cons (cons (car xs) pass) rest))
      (cons nil xs)))
