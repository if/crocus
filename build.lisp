;; build script for crocus
(ql:quickload :crocus)

(sb-ext:save-lisp-and-die "crocus"
                          :toplevel 'crocus:main
                          :executable t)
