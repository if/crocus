;; store.lisp
;; variable storage with lexical scope
;; this is definitely a dorohedoro reference btw

(in-package #:crocus)


;; === variable store ===
;; * keeps track of currently defined variables using lexical scope
;; * depth: current scope depth (0 = top-level)
;; * size: number of variables in current scope
;; * store: an alist of (name . (depth . data)) structures
(defclass <var-store> ()
  ((depth :accessor depth :initarg :depth :initform 0)
   (size  :accessor size  :initarg  :size :initform 0)
   (store :accessor store :initarg :store :initform nil)))

(defun init-var-store ()
  "make an empty `<var-store>'"
  (make-instance '<var-store>))

(defmethod +depth ((vars <var-store>))
  "increment lexical scope depth"
  (setf (depth vars) (1+ (depth vars))))

(defmethod -depth ((vars <var-store>))
  "decrement lexical scope depth and return previous scope"
  ;; remove old bindings
  ;; decr depth if greater than 0
  (when (> (depth vars) 0)
    (let* ((parts (split-while (lambda (v)
                                 (>= (cadr v) (depth vars)))
                               (store vars)))
           (culled (car parts))
           (new-store (cdr parts)))
      (setf (store vars) new-store)
      (setf (size vars) (- (size vars) (length culled)))
      (setf (depth vars) (1- (depth vars)))
      culled)))


;; note: variable names are interned, which means two strings
;; with equal contents are considered equal when doing lookup
(defmethod store-var ((vars <var-store>) name data)
  "store a variable, its data, and its depth in `*vars*'"
  (let* ((record (cons (intern name) (cons (depth vars) data))))
    (debug-log "[~a] ~a => ~a~%" (depth vars) name data)
    (push record (store vars))
    (setf (size vars) (1+ (size vars)))))

;; note: this function relies on the fact that "deeper" bindings
;; are stored at the front of the list
(defmethod lookup-var ((vars <var-store>) name)
  "look up a variable's data in the current lexical scope"
  (let ((binding (find-if (lambda (var) (string= (car var) name)) (store vars))))
    (if binding (cddr binding)
        (error (format nil "undefined variable: `~a'~%" name)))))
