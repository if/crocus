;;; parse.lisp
;;; parses crocus source code
(in-package #:crocus)

;; although crocus syntax looks lisp-esque right now,
;; crocus is *not* a lisp. that's why things like
;; `let' and `var' are parsed separately, because macros
;; and cons cells and so on aren't part of crocus.

(defrule whitespace ()
  (postimes (|| #\space #\tab #\newline)))

(defrule initial-char ()
  (|| (character-ranges (#\a #\z) (#\A #\Z))
    #\! #\@ #\$ #\% #\^ #\& #\* #\. #\/ #\,
    #\? #\/ #\\ #\: #\| #\= #\+ #\- #\_))


(defun not-doublequote (ch)
  (not (eql #\" ch)))

(defrule str-char ()
  (|| (esrap-liquid:pred #'not-doublequote character)
    (list (v #\\) (v #\"))))

(defrule str ()
  (cons :str (text (esrap-liquid:progm #\" (times str-char) #\"))))


(defrule digit ()
  (character-ranges (#\0 #\9)))

(defrule subsequent-char ()
  (|| initial-char #\# digit))

(defrule num ()
  (cons :number (parse-float:parse-float
                 (text (list (? (|| #\+ #\-))
                             (postimes digit)
                             (? (list (v #\.) (times digit))))))))

(defrule name ()
  (text (list (v initial-char) (times subsequent-char))))

(defrule sym ()
  (cons :sym (v name)))

(defrule crunch ()
  (let ((c (text (list (v #\#) (v name)))))
    (cons :crunch (cond ((string= c "#t") t)
                        ((string= c "#f") nil)
                        (t c)))))



(defmacro parens (rule)
  `(prog1 (progn (? whitespace) (v #\()
                 (? whitespace) ,rule)
     (? whitespace) (v #\))))

(defmacro pointy (rule)
  `(prog1 (progn (? whitespace) (v #\<)
                 (? whitespace) ,rule)
     (? whitespace) (v #\>)))


(defrule var ()
  (parens (list (v "var") (v whitespace)
                (esrap-liquid:cap a name) (v whitespace)
                (esrap-liquid:cap b term)))
  (list* :var-decl (esrap-liquid:recap a) (esrap-liquid:recap b)))

(defmacro list-of (rule)
  `(let ((xs (list (? ,rule) (times (progn (v whitespace) ,rule)))))
     (cons (car xs) (cadr xs))))


(defrule tuple ()
  (cons :tuple (pointy (list-of (v term)))))


(defrule params () (parens (list-of (v name))))
(defrule binding ()
  (parens (list (esrap-liquid:cap a name)
                (v whitespace)
                (esrap-liquid:cap b term)))
  (cons (esrap-liquid:recap a) (esrap-liquid:recap b)))

(defrule if* () (parens (list (v "if") (v whitespace)
                              (esrap-liquid:cap test term)
                              (v whitespace)
                              (esrap-liquid:cap true-branch term)
                              (v whitespace)
                              (esrap-liquid:cap false-branch term)))
         (list* :if (esrap-liquid:recap test)
           (esrap-liquid:recap true-branch)
           (esrap-liquid:recap false-branch)))

(defrule term () (|| str num sym crunch if* let** lambda* apply* tuple))
(defrule form () (|| var term))
(defrule forms () (times form))

(defrule let** ()
  (parens (list (v "let") (v whitespace)
                (esrap-liquid:cap vars (parens (list-of (v binding))))
                (v whitespace) (esrap-liquid:cap body term)))
  (list* :let (esrap-liquid:recap vars) (esrap-liquid:recap body)))

(defrule lambda* ()
  (parens (list (|| "lambda" #\greek_small_letter_lambda)
                (v whitespace) (esrap-liquid:cap vars params)
                (v whitespace) (esrap-liquid:cap body term)))
  (list* :lambda (car (esrap-liquid:recap vars)) (esrap-liquid:recap body)))

(defrule apply* ()
  (parens (list (esrap-liquid:cap fn term) (? whitespace)
                (esrap-liquid:cap args (list-of (v term)))))
  (list* :apply (esrap-liquid:recap fn) (esrap-liquid:recap args)))

