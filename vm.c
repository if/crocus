#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "debug.h"
#include "thing.h"
#include "memory.h"
#include "vm.h"

VM vm;

void reset_stack() {
  // set top of stack to bottom of stack
  vm.stack_top = vm.stack;
  vm.num_frames = 0;
}

void init_vm() {
  reset_stack();
  vm.things = NULL;
  init_table(&vm.strings);
  init_table(&vm.globals);
}

void free_vm() {
  free_table(&vm.strings);
  free_table(&vm.globals);
  free_things();
}

value peek(int offset) {
  return vm.stack_top[-1 - offset];
}

static void panic(const char *format, ...) {
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
  fputs("\n", stderr);

  call_frame *frame = &vm.frames[vm.num_frames-1];
  size_t instr = frame->ip - frame->closure->fun->chunk.code - 1;
  int line = frame->closure->fun->chunk.lines[instr];
  fprintf(stderr, "at line %d\n", line);

  reset_stack();
}


static void concatenate() {
  thing_str *b = AS_STR(pop());
  thing_str *a = AS_STR(pop());

  int len = a->len + b->len;
  char *chars = ALLOC(char, len+1);
  memcpy(chars, a->chars, a->len);
  memcpy(chars + a->len, b->chars, b->len);
  chars[len] = '\0';

  thing_str *result = take_str(chars, len);
  push(THING_VAL(result));
}



static bool call(thing_closure *closure, int num_args) {
  /* initialize a new call frame */
  call_frame *frame = &vm.frames[vm.num_frames++];
  frame->closure = closure;
  frame->ip = closure->fun->chunk.code;
  /* slots are (num_args+1) spaces back from the top;
   the (+1) part is for the function itself */
  frame->stack = vm.stack_top - (num_args+1);
  return true;
}


static bool call_value(value callee, int num_args) {
  if (IS_THING(callee) && THING_TYPE(callee) == THING_CLOSURE)
    return call(AS_CLOSURE(callee), num_args);

  
  /* otherwise `callee' wasn't a function */
  printf("callee: ");
  print_value(callee);
  panic("tried to call something that wasn't a function!");
  return false;
}


static thing_upval *capture_upval(value *local) {
  thing_upval *uv = new_upval(local);
  return uv;
}



static interpret_result run() {
  call_frame *frame = &vm.frames[vm.num_frames-1];

#define READ_BYTE() (*frame->ip++)
#define READ_SHORT() \
  (frame->ip += 2, (uint16_t)((frame->ip[-2] << 8) | frame->ip[-1]))
#define READ_CONST() (frame->closure->fun->chunk.consts.values[READ_BYTE()])
#define READ_STR() AS_STR(READ_CONST())
#define BINARY_OP(value_type, op)               \
  do {                                          \
    if (!IS_NUM(peek(0)) || !IS_NUM(peek(1))) { \
      panic("operands must be numbers");        \
      return INTERPRET_ERR;                     \
    }                                           \
    double b = AS_NUM(pop());                   \
    double a = AS_NUM(pop());                   \
    push(value_type(a op b));                   \
  } while(false)
  /* ^ dead loop helps constrain scope */

    
  while (1) {
#ifdef DEBUG_TRACE_EXECUTION
    printf("            ");
    for (value *slot = vm.stack; slot < vm.stack_top; slot++) {
      printf("[ ");
      print_value(*slot);
      printf(" ]");
    }
        
    puts("");
        
    disassemble_instr(&frame->closure->fun->chunk,
                      (int)(frame->ip - frame->closure->fun->chunk.code));
#endif
        
    uint8_t instr;
    switch (instr = READ_BYTE()) {
    case OP_PRINT: {
      print_value(pop());
      puts("");
      break;
    }

    case OP_POP: pop(); break;
      
    case OP_DGLOBAL: {
      thing_str *name = READ_STR();
      table_set(&vm.globals, name, peek(0));
      // pop after table_set in case table resizes itself
      pop();
      break;
    }

    case OP_LGLOBAL: {
      thing_str *name = READ_STR();
      value val;
      
      if (!table_get(&vm.globals, name, &val)) {
        panic("undefined variable %s\n", name->chars);
        return INTERPRET_ERR;
      }

      push(val);
      break;
    }


    case OP_DLOCAL: {
      uint8_t slot = READ_BYTE();
      /* peek is necessary so vm remembers
         that a variable lives in `slot' */
      frame->stack[slot] = peek(0);
      break;
    }

    case OP_LLOCAL: {
      /* load local from stack @ slot */
      uint8_t slot = READ_BYTE();
      push(frame->stack[slot]);
      break;      
    }
      
    case OP_CONSTANT: {
      value constant = READ_CONST();
      push(constant);
      break;
    }
            
    case OP_NEGATE: {
      if(!IS_NUM(peek(0))) {
        panic("operand must be a number");
        return INTERPRET_ERR;
      }
            
      push((NUM_VAL(-AS_NUM(pop()))));
      break;
    }

    case OP_CAT: concatenate(); break;
    case OP_ADD: BINARY_OP(NUM_VAL, +); break;
    case OP_SUBTRACT: BINARY_OP(NUM_VAL, -); break;
    case OP_MULTIPLY: BINARY_OP(NUM_VAL, *); break;
    case OP_DIVIDE: BINARY_OP(NUM_VAL, /); break;
    case OP_GT: BINARY_OP(BOOL_VAL, >); break;
    case OP_LT: BINARY_OP(BOOL_VAL, <); break;
    case OP_NIL: push(NIL_VAL); break;
    case OP_TRUE: push(BOOL_VAL(true)); break;
    case OP_FALSE: push(BOOL_VAL(false)); break;
    case OP_NOT:
      push(BOOL_VAL(!AS_BOOL(pop())));
      break;
    case OP_EQ: {
      value b = pop();
      value a = pop();
      push(BOOL_VAL(values_equal(a, b)));
      break;
    }

      /* `and' and `or' aren't lazy in crocus because
         functions of type (bool, bool) -> bool don't
         have side effects, so it shouldn't matter */
    case OP_AND: {
      bool b = AS_BOOL(pop());
      bool a = AS_BOOL(pop());
      push(BOOL_VAL(a && b));
      break;
    }

    case OP_OR: {
      bool b = AS_BOOL(pop());
      bool a = AS_BOOL(pop());
      push(BOOL_VAL(a || b));
      break;
    }
      
    case OP_JUMP: {
      uint16_t offset = READ_SHORT();
      frame->ip += offset;
      break;
    }
      
    case OP_JUMPZ: {
      uint16_t offset = READ_SHORT();
      value predicate = pop();
      if (IS_BOOL(predicate) && !AS_BOOL(predicate))
        frame->ip += offset;
      break;
    }

    case OP_CALL: {
      int num_args = READ_BYTE();
      /* call the function */
      if (!call_value(peek(num_args), num_args)) {
        return INTERPRET_ERR;
      }

      /* go back to previous call frame */
      frame = &vm.frames[vm.num_frames-1];
      break;
    }

    case OP_RETURN: {
      /* save returned value */
      value result = pop();
      vm.num_frames--;
      /* if returning from entire program */
      if (vm.num_frames == 0) {
        pop();
        return INTERPRET_OK;
      }

      /* get rid of prevous call frame */
      vm.stack_top = frame->stack;
      /* push returned value back on */
      push(result);
      frame = &vm.frames[vm.num_frames-1];
      break;
    }

    case OP_TUP_GET: {
      /* push the i'th element of a tuple onto the stack */
      thing_tup *tuple = AS_TUP(pop());
      int index = READ_BYTE();

      if (index >= tuple->len)
        panic("tuple index out of bounds!!!");

      push(tuple->vals[index]);

      break;
    }

    case OP_TUP_SET: {
      int index = READ_BYTE();
      value elem = pop();
      /* keep tuple on stack */
      /* this makes consecutive TUP_SETs simpler */
      thing_tup *tuple = AS_TUP(pop());

      if (index >= tuple->len)
        panic("tuple index out of bounds!!!");

      tuple->vals[index] = elem;
      push(THING_VAL(tuple));
      break;
    }


    case OP_CLOSURE: {
      /* convert function to closure */
      thing_fun *fun = AS_FUN(READ_CONST());
      thing_closure *closure = new_closure(fun);
      push(THING_VAL(closure));

      for (int i = 0; i < closure->num_upvals; i++) {
        uint8_t localp = READ_BYTE();
        uint8_t  index = READ_BYTE();
        /* either capture local in surrounding scope,
           or "inherit" upval from current function */
        if (localp)
          closure->upvals[i] =
            capture_upval(frame->stack+index);
        else
          closure->upvals[i] =
            frame->closure->upvals[index];
      }

      break;
    }

    case OP_GET_UPVALUE: {
      uint8_t slot = READ_BYTE();
      push(*frame->closure->upvals[slot]->location);
      break;
    }

    case OP_SET_UPVALUE: {
      uint8_t slot = READ_BYTE();
      *frame->closure->upvals[slot]->location = peek(0);
      break;
    }

    case OP_SET_UPVALUE:
      close_upvals(vm.stack_top-1);
      pop();
      break;
    }
  }

#undef BINARY_OP
#undef READ_CONST
#undef READ_STR
#undef READ_SHORT
#undef READ_BYTE
}



interpret_result interpret(thing_fun *entry) {
  /* push the entry point and then call it */
  thing_closure *_entry = new_closure(entry);
  push(THING_VAL(_entry));
  call(_entry, 0);
  return run();
}

void push(value val) {
  *(vm.stack_top) = val;
  vm.stack_top++;
}

value pop() {
  vm.stack_top--;
  return *(vm.stack_top);
}

