;;;; package.lisp

(defpackage #:crocus
  (:use #:cl)
  (:export #:main)
  (:shadowing-import-from #:cffi
                          #:defcstruct
                          #:defcenum
                          #:defcunion
                          #:defcfun
                          #:defcvar)
  (:shadowing-import-from #:esrap-liquid
                          #:defrule
                          #:parse
                          #:||
                          #:times
                          #:postimes
                          #:character-ranges
                          #:v
                          #:?
                          #:text)
  (:shadowing-import-from #:cl-unification
                          #:unify
                          #:make-empty-environment
                          #:find-variable-value))
