#ifndef __TAB_H__
#define __TAB_H__

#include "common.h"
#include "value.h"


typedef struct {
  thing_str *key;
  value value;
} entry;


typedef struct {
  int count;
  int capacity;
  entry *entries;
} table;



void init_table(table *tab);
void free_table(table *tab);
bool table_set(table *tab, thing_str *key, value val);
void table_add_all(table *from, table *to);
bool table_get(table *tab, thing_str *key, value *val);
bool table_delete(table *tab, thing_str *key);
thing_str *table_find_str(table *tab, const char *chars,
                          int len, uint32_t hash);
void print_table(table *tab);

#endif
