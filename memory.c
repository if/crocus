 #include <stdlib.h>
#include "memory.h"
#include "vm.h"
#include "value.h"

void *reallocate(void *ptr, size_t old_size, size_t new_size) {
    if (new_size == 0) {
        free(ptr);
        return NULL;
    }

    void *result = realloc(ptr, new_size);
    if (result == NULL) exit(1);
    return result;
}

static void free_thing(thing *thing) {
    switch (thing->type) {
    case THING_STR: {
        thing_str *str = (thing_str*)thing;
        FREE_ARRAY(char, str->chars, str->len+1);
        FREE(thing_str, thing);
        break;
    }

    case THING_FUN: {
      thing_fun *fun = (thing_fun*)thing;
      free_chunk(&fun->chunk);
      FREE(thing_fun, fun);
      break;
    }

    case THING_CLOSURE: {
      thing_closure *closure = (thing_closure*)thing;
      FREE(thing_closure, thing);
      FREE_ARRAY(thing_upval*, closure->upvals,
                 closure->num_upvals);
      /* don't free the closure's function, because
         the function isn't "owned" by the closure */
      break;
    }


    case THING_UPVAL: {
      FREE(thing_upval, thing);
      /* don't free the upval's variable, because
         the upval itself doesn't "own" it */
      break;
    }

    case THING_TUP: {
      thing_tup *tup = (thing_tup*)thing;
      FREE_ARRAY(int, tup->vals, tup->len);
      FREE(thing_tup, thing);
      break;
    }
    }
}

void free_things() {
    thing *to_delete = vm.things;

    while (to_delete != NULL) {
        thing* next = to_delete->next;
        free_thing(to_delete);
        to_delete = next;
    }
}
