#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "thing.h"
#include "memory.h"
#include "value.h"


/* some functions for dynamically allocating values, because
   sbcl's cffi has an easier time handling pointers */
value *make_bool_value(bool b) {
    value *val = malloc(sizeof(val));
    val->type = VALUE_BOOL;
    val->as.boolean = b;

    return val;
}

value *make_nil_value() {
    value *val = malloc(sizeof(val));
    val->type = VALUE_NIL;
    val->as.num = 0;

    return val;
}

value *make_num_value(double num) {
    value *val = malloc(sizeof(val));
    val->type = VALUE_NUM;
    val->as.num = num;

    return val;
}

value *make_str_value(char *chars, int len) {
  thing_str *th = take_str(chars, len);
    
  value *val = malloc(sizeof(val));
  val->type = VALUE_THING;
  val->as.thing = (thing*)th;
    
  return val;
}

value *make_thing_value(thing *th) {
  value *val = malloc(sizeof(val));
  val->type = VALUE_THING;
  val->as.thing = th;
  
  return val;
}


value_array *make_value_array(int cap, int count, value *vals) {
    value_array *arr = malloc(sizeof(value_array));
    arr->capacity = cap;
    arr->count = count;
    arr->values = vals;
    
    return arr;
}

void init_value_array(value_array *arr) {
    arr->values = NULL;
    arr->capacity = 0;
    arr->count = 0;
}

void write_value_array(value_array *arr, value val) {
    if (arr->capacity < (arr->count + 1)) {
        int old_capacity = arr->capacity;
        arr->capacity = GROW_CAPACITY(old_capacity);
        arr->values = GROW_ARRAY(value, arr->values,
                                 old_capacity, arr->capacity);
    }

    arr->values[arr->count] = val;    
    arr->count++;
}

void free_value_array(value_array *arr) {
    FREE_ARRAY(value, arr->values, arr->capacity);
    init_value_array(arr);
}

void print_value(value val) {
    switch(val.type) {
    case VALUE_BOOL:
        printf(AS_BOOL(val)? "true" : "false");
        break;
    case VALUE_NIL: printf("nil"); break;
    case VALUE_NUM: printf("%g", AS_NUM(val)); break;
    case VALUE_THING: print_thing(val); break;
    }
}

/* print_value, but takes a pointer */
void print_value_compat(value *val) {
  print_value(*val);
}

/** get a value; do a bounds check on index */
value value_at(value_array arr, uint8_t index) {
    if (index >= arr.count) {
        fprintf(stderr, "bad constant index: %d\n", index);
        fprintf(stderr, "stopping.\n");
        exit(1);
    }

    return arr.values[index];
}


bool values_equal(value a, value b) {
    if (a.type != b.type) return false;

    switch (a.type) {
    case VALUE_BOOL:
        return AS_BOOL(a) == AS_BOOL(b);
    case VALUE_NUM:
        return AS_NUM(a) == AS_NUM(b);
    case VALUE_NIL:
        return true;
    case VALUE_THING:
      /* strings are interned, so equal strings must 
         necessarily be the same part of memory */
      return AS_THING(a) == AS_THING(b);
    default:
        return false;
    }
}
