;;;; crocus.asd

(asdf:defsystem #:crocus
  :description "A programming language"
  :author "undefined"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on (#:cl-unification
               #:esrap-liquid
               #:unix-opts
               #:cffi
               #:parse-float
               #:cl-readline)
  :components ((:file "package")
               (:file "util")
               (:file "store")
               (:file "crocus")
               (:file "parse")
               (:file "log")
               (:file "types")))
