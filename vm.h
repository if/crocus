#ifndef __VM_H__
#define __VM_H__

#include "chunk.h"
#include "thing.h"
#include "tab.h"
#include "value.h"

#define FRAME_MAX 0x40
/* must be big enough for FRAME_MAX */
#define STACK_MAX (FRAME_MAX * UINT8_COUNT)


/* there's no distinction between
   runtime errors and compile errors
   because the lisp "frontend" handles
   all the compilation stuff */
typedef enum {
  INTERPRET_OK,
  INTERPRET_ERR,
} interpret_result;


/* function call frame */
typedef struct {
  thing_closure *closure;
  /* ip of function (NOT return addr) */
  uint8_t *ip;
  /* start of the stack available to fun */
  value *stack;
} call_frame;


typedef struct {
  chunk *chunk;
  /* ip points to next instr */
  uint8_t *ip;
  value stack[STACK_MAX];
  /* next available space in stack */
  value *stack_top;
  call_frame frames[FRAME_MAX];
  int num_frames;
  /* list of dynamically allocated "things" */
  thing *things;
  /* interned strings */
  table strings;
  table globals;
} VM;


extern VM vm;


void init_vm();
void free_vm();
void reset_stack();
interpret_result interpret(thing_fun *entry);
void push(value val);
value pop();
value peek(int offset);


#endif
