#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "chunk.h"

void disassemble_chunk(chunk *chunk, const char *name);
int disassemble_instr(chunk *chunk, int offset);


#endif
