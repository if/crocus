 #ifndef __THING_H__
#define __THING_H__
#include "common.h"
#include "chunk.h"
/* contains thing typedef */
#include "value.h"


typedef enum {
    THING_STR,
    THING_FUN,
    THING_CLOSURE,
    THING_UPVAL,
    THING_TUP,
} thing_type;

/* thing */
#define THING_TYPE(val)    (AS_THING(val)->type)
void print_thing(value val);

struct thing {
    thing_type type;
    struct thing *next;
};

static inline bool isThingType(value val, thing_type type) {
    return IS_THING(val) && AS_THING(val)->type == type;
}




/* string */
#define IS_STR(val)        isThingType(val, THING_STR)
#define  AS_STR(val)        ((thing_str*)AS_THING(val))
#define AS_CSTR(val)        (((thing_str*)AS_THING(val))->chars)
thing_str *take_str(char *chars, int len);

struct thing_str {
  thing thing;
  int len;
  char *chars;
  uint32_t hash;
};




/* function */
#define IS_FUN(val)        isThingType(val, THING_FUN)
#define AS_FUN(val)        ((thing_fun*)AS_THING(val))
thing_fun *new_fun();
chunk *chunk_from_fun(thing_fun *fun);

struct thing_fun {
  thing thing;
  int arity;
  chunk chunk;
  int num_upvalues;
};


/* closure */
#define IS_CLOSURE(val)    isThingType(val, THING_CLOSURE)
#define AS_CLOSURE(val)    ((thing_closure*)AS_THING(val))
thing_closure *new_closure(thing_fun *fun);

struct thing_closure {
  thing thing;
  thing_fun *fun;
  int num_upvals;
  thing_upval **upvals;
};


/* upvalue */
thing_upval *new_upval(value *slot);

struct thing_upval {
  thing thing;
  value *location;
  struct thing_upval *saved;            /* for upvals from out of scope */
};



/* tuple */
#define IS_TUP(val)        isThingType(val, THING_TUP)
#define AS_TUP(val)        ((thing_tup*)AS_THING(val))
thing_tup *make_tuple(int len);
value     *make_tuple_value(int len);
/* a tuple is just a fixed-length array of vals */
struct thing_tup {
  thing thing;
  int len;
  value *vals;
};




#endif
