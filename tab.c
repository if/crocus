#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "memory.h"
#include "thing.h"
#include "tab.h"
#include "value.h"

#define TAB_MAX_LOAD 0.75


void init_table(table *tab) {
  tab->count = 0;
  tab->capacity = 0;
  tab->entries = NULL;
}

void free_table(table *tab) {
  FREE_ARRAY(entry, tab->entries, tab->capacity);
  init_table(tab);
}

/* find the appropriate bucket for a key, using linear probing */
static entry *find_entry(entry *entries, int capacity,
                         thing_str *key) {
  uint32_t index = key->hash % capacity;
  /* the last tombstone entry visited */
  entry *tombstone = NULL;

  /* load factor limit prevents this from being an infinite loop */
  while (true) {
    entry *entry = &entries[index];

    if (entry->key == NULL) {
      /* empty entry (reuse last tombstone) */
      if (IS_NIL(entry->value)) {
        return tombstone != NULL ? tombstone : entry;
      } else {
        /* tombstone */
        if (tombstone == NULL)
          tombstone = entry;
      }
    } else if (entry->key == key) {
      /* return the entry if we found its key */
      return entry;
    }

    /* otherwise, look at next entry */
    index = (index+1) % capacity;
  }
}


static void adjust_capacity(table *tab, int new_capacity) {
  entry *entries = ALLOC(entry, new_capacity);

  for (int i = 0; i < new_capacity; i++) {
    entries[i].key = NULL;
    entries[i].value = NIL_VAL;
  }

  
  /* put entries in new array (indices will be different) */
  /* also reset the count since the tombstones aren't copied */
  tab->count = 0;
  for (int i = 0; i < tab->capacity; i++) {
    entry *_entry = &tab->entries[i];
    if (_entry->key == NULL) continue;

    entry *dest = find_entry(entries, new_capacity, _entry->key);
    dest->key = _entry->key;
    dest->value = _entry->value;
    tab->count++;
  }
  
  /* free old entries array */
  FREE_ARRAY(entry, tab->entries, tab->capacity);

  tab->entries = entries;
  tab->capacity = new_capacity;
}


/* add a k-v pair to the table; returns true if this
   overwrites an existing k-v' pair */
bool table_set(table *tab, thing_str *key, value val) {
  /* grow the table if load factor gets too high */
  if ((tab->count + 1) > (tab->capacity * TAB_MAX_LOAD)) {
    int new_capacity = GROW_CAPACITY(tab->capacity);
    adjust_capacity(tab, new_capacity);
  }


  entry *entry = find_entry(tab->entries, tab->capacity, key);
  bool is_new_key = (entry->key == NULL);
  /* increment count if inserting into empty bucket 
     (i.e. not reusing a tombstone bucket) */
  if (is_new_key && IS_NIL(entry->value))
    tab->count++;

  entry->key = key;
  entry->value = val;
  
  return is_new_key;
}


void table_add_all(table *from, table *to) {
  for (int i = 0; i < from->capacity; i++) {
    entry *entry = &from->entries[i];
    if (entry->key != NULL)
      table_set(to, entry->key, entry->value);
  }
}


/* returns `true' and sets `val' if `key' is found */
bool table_get(table *tab, thing_str *key, value *val) {
  if (tab->count == 0) return false;

  entry *entry = find_entry(tab->entries, tab->capacity, key);
  if (entry->key == NULL) return false;

  *val = entry->value;
  return true;
}


bool table_delete(table *tab, thing_str *key) {
  if (tab->count == 0) return false;

  entry *entry = find_entry(tab->entries, tab->capacity, key);
  if (entry->key == NULL) return false;

  /* place a "tombstone" at the entry, to preserve the 
     linear probing chains */
  entry->key = NULL;
  entry->value = BOOL_VAL(true);
  return true;
}


thing_str *table_find_str(table *tab, const char *chars,
                          int len, uint32_t hash) {
  if (tab->count == 0) return NULL;

  uint32_t index = hash % tab->capacity;

  while (true) {
    entry *entry = &tab->entries[index];

    if (entry->key == NULL) {
      /* string isn't in table */
      if (IS_NIL(entry->value))
        return NULL;
    } else if (entry->key->len == len
               && entry->key->hash == hash
               && memcmp(entry->key->chars, chars, len) == 0) {
      /* string is in table */
      return entry->key;
    }

    index = (index + 1) % tab->capacity;
  }
}



/* for debugging -- probably kind of broken */
void print_table(table *tab) {
  printf("count: %d; cap: %d\n", tab->count, tab->capacity);
  for (int i = 0; i < tab->capacity; i++) {
    entry e = tab->entries[i];
    if (e.key != NULL)
      printf("[%d] %s @ x%x\n", i, e.key->chars,
             e.key->chars);
  }
}
