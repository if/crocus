;;; log.lisp
;;; utils for logging
(in-package #:crocus)

(defparameter *log-output* t
  "where to send logging output")

(defun debug-log (&rest args)
  "like `format', but output is dynamically controlled via `*log-output*'"
  (apply #'format (cons *log-output* args)))
