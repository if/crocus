#include <stdio.h>
#include <stdlib.h>
#include "chunk.h"
#include "memory.h"

chunk *make_chunk() {
  chunk *c = malloc(sizeof(chunk));
  init_chunk(c);
  return c;
}

void delete_chunk(chunk *chunk) {
  free_chunk(chunk);
  free(chunk);
}


void init_chunk(chunk *chunk) {
  chunk->count = 0;
  chunk->capacity = 0;
  chunk->code = NULL;
  chunk->lines = NULL;
  init_value_array(&chunk->consts);
}

void write_chunk(chunk *chunk, uint8_t byte, int line) {
  if (chunk->capacity < (chunk->count + 1)) {
    int old_capacity = chunk->capacity;
    chunk->capacity = GROW_CAPACITY(old_capacity);
    chunk->code = GROW_ARRAY(uint8_t, chunk->code,
                             old_capacity, chunk->capacity);
    chunk->lines = GROW_ARRAY(int, chunk->lines,
                              old_capacity, chunk->capacity);
  }

  chunk->code[chunk->count] = byte;
  chunk->lines[chunk->count] = line;
  chunk->count++;
}

/** go `offset' bytes back into `chunk's code and overwrite it */
void overwrite_chunk(chunk *chunk, uint8_t byte, int offset) {
  int idx = chunk->count - offset;

  if (idx < 0) {
    fprintf(stderr, "bad code index: %d\n", idx);
    exit(1);
  }

  chunk->code[idx] = byte;
}

/* "empties out" a chunk's contents */
void free_chunk(chunk *chunk) {
  FREE_ARRAY(uint8_t, chunk->code, chunk->capacity);
  FREE_ARRAY(int, chunk->lines, chunk->capacity);
  free_value_array(&chunk->consts);
  init_chunk(chunk);
}

/* returns index of const added */
int add_const(chunk *chunk, value val) {
  write_value_array(&chunk->consts, val);
  return chunk->consts.count - 1;
}

/* like add_const, but takes a pointer for `val' in order
   to be compatible with the lisp frontend */
int add_const_compat(chunk *chunk, value *val) {
  return add_const(chunk, *val);
}
