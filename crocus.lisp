(in-package #:crocus)

(cffi:define-foreign-library crocus-vm
  (t (:default "./crocus")))

(cffi:use-foreign-library crocus-vm)


(defcvar ("stdout" stdout) :pointer)


(defcenum value-type
  :value-num
  :value-nil
  :value-bool
  :value-thing)

(defcunion value-form
  (boolean :bool)
  (num :double)
  (thing :pointer))

(defcstruct value
  (type :int)
  (as (:union value-form)))


(defcenum opcode
  :op-return
  :op-constant
  :op-true
  :op-false
  :op-nil
  :op-negate
  :op-add
  :op-subtract
  :op-multiply
  :op-divide
  :op-not
  :op-and
  :op-or
  :op-eq
  :op-gt
  :op-lt
  :op-cat
  :op-print
  :op-pop
  :op-dglobal
  :op-lglobal
  :op-dlocal
  :op-llocal
  :op-jump
  :op-jumpz
  :op-call
  :op-closure
  :op-get-upvalue
  :op-set-upvalue
  :op-close-upvalue
  :op-tup-get
  :op-tup-set)


(defcfun "make_chunk" :pointer)
(defcfun "make_num_value" :pointer (num :double))
(defcfun "make_bool_value" :pointer (b :int))
(defcfun "make_str_value" :pointer (chars :string) (len :int))
(defcfun "make_thing_value" :pointer (th :pointer))
(defcfun "write_chunk" :void (chunk :pointer) (byte :uint8) (line :int))
(defcfun "overwrite_chunk" :void (chunk :pointer) (byte :uint8) (offset :int))
(defcfun "add_const_compat" :int (chunk :pointer) (val :pointer))
(defcfun "new_fun" :pointer)
(defcfun "chunk_from_fun" :pointer (fun :pointer))
(defcfun "make_tuple_value" :pointer (len :int))
(defcfun "disassemble_chunk" :void (chunk :pointer) (name :string))
(defcfun "run_chunk" :int (chunk :pointer))
(defcfun "print_value_compat" :void (value :pointer))
(defcfun "init_vm" :void)


;; store of local variable addresses
(defparameter *addr-store* (init-var-store))


;; emissive functions, but patched to record emesis
(defun write-chunk- (chunk byte line)
  "`write-chunk' but also increments `*emesis*'"
  (write-chunk chunk byte line)
  (+emesis))

;; records the number of bytes that have been emitted;
;; this is used for backpatching offsets in jump instrs
(defparameter *emesis* 0)

(defun reset-emesis ()
  "set `*emesis*' to 0"
  (setq *emesis* 0))

(defun +emesis ()
  "increment `*emesis*'"
  (setq *emesis* (1+ *emesis*)))

(defmacro get-emesis (&body body)
  "measure the amount of bytecode emitted by `body'"
  `(let ((e1 *emesis*))
     (progn
       ,@body
       (- *emesis* e1))))


(defun emit (chunk op &optional (line 0))
  "emit an opcode into `chunk'"
  (write-chunk- chunk (cffi:foreign-enum-value 'opcode op) line))





(defun compile-builtin (chunk expr)
  "compile a call to a built-in function"
  (cond ((string= expr "+") (emit chunk :op-add))
        ((string= expr "-") (emit chunk :op-subtract))
        ((string= expr "*") (emit chunk :op-multiply))
        ((string= expr "/") (emit chunk :op-divide))
        ;; TODO: detect negative numbers in parser
        ((string= expr "negate") (emit chunk :op-negate))
        ((string= expr "=") (emit chunk :op-eq))
        ((string= expr ">") (emit chunk :op-gt))
        ((string= expr "<") (emit chunk :op-lt))
        ((string= expr "not") (emit chunk :op-not))
        ((string= expr "cat") (emit chunk :op-cat))
        ((string= expr "print") (emit chunk :op-print))
        ((string= expr "and") (emit chunk :op-and))
        ((string= expr  "or")  (emit chunk :op-or))
        (t (error (format nil "not a builtin function: ~a~%" expr)))))

(defun compile-apply (chunk exprs)
  "compile a function (or macro) application"
  (let ((fn (car exprs))
        (args (cdr exprs)))
    (debug-log "compile-call ~a~%" fn)
    (if (and (stringp (cdr fn))
             (builtinp (cdr fn)))
        (progn (mapcar (lambda (arg) (compile-chunk chunk arg)) args)
               (compile-builtin chunk (cdr fn)))
        (progn (compile-chunk chunk fn)
               (mapcar (lambda (arg) (compile-chunk chunk arg)) args)
               (emit chunk :op-call)
               ;; TODO: change "1" to actual arity
               ;; once you get past simple l-funs
               (write-chunk- chunk 1 0)))))

(defun const->val (expr)
  "convert a constexpr to a `value' pointer"
  (let ((type (car expr))
        (const (cdr expr)))
    (case type
      (:number (make-num-value (coerce const 'double-float)))
      (:bool (make-bool-value (if const 1 0)))
      (otherwise (error (format nil "not a const type: ~a~%" type))))))

(defun compile-const (chunk const)
  "compile a constant value"
  (let ((index (add-const-compat chunk const)))
    (debug-log "writing ~a @ ~a~%" const index)
    (emit chunk :op-constant)
    (write-chunk- chunk index 0)))

(defun write-str-value (str chunk line)
  "writes the string `str' to `chunk'"
  (let* ((fstr (cffi:foreign-string-alloc str))
         (val (make-str-value fstr (length str)))
         (index (add-const-compat chunk val)))
    (debug-log "writing '~a' @ ~a~%" str index)
    (write-chunk- chunk index line)))

(defun compile-str (chunk str const-p)
  "compile a string constant"
  (when const-p (emit chunk :op-constant))
  (write-str-value str chunk 0))


;; (defun compile-decl (chunk expr)
;;   "compile a global variable declaration."
;;   (let* ((name (cdar expr))
;;          (val (cdr expr)))
;;     (debug-log "compile-decl  ~a => ~a~%" name val)
;;     (compile-chunk chunk val)
;;     (emit chunk :op-dglobal)
;;     (compile-str chunk name nil)))
;; NOTE: this version stores vars as local to entry function
(defun compile-decl (chunk expr)
  "compile a variable declaration."
  (let* ((name (car expr))
         (val (cdr expr))
         (slot (1+ (size *addr-store*))))
    (store-var *addr-store* name slot)
    (debug-log "compile-decl  ~a => ~a~%" name val)
    (compile-chunk chunk val)
    (emit chunk :op-dlocal)
    (write-chunk- chunk slot 0)))

;; (defun compile-variable (chunk name)
;;   "compile a reference to a local or global variable"
;;   (emit chunk :op-lglobal)
;;   (compile-str chunk name nil))
(defun compile-variable (chunk name)
  "compile a variable reference"
  (let* ((slot (lookup-var *addr-store* name)))
    (emit chunk :op-llocal)
    (write-chunk- chunk slot 0)))






(defun write-bogus-offset (chunk)
  "write a bogus offset (takes up 2 bytes)"
  (write-chunk- chunk #xc0 0)
  (write-chunk- chunk #xfe 0))

(defun backpatch-offset (chunk offset jump-back)
  "write a jump offset as two separate bytes"
  ;; make sure both offsets fit inside a short
  (when (> offset #xffff)
    (error (format nil "offset is too large: ~a~%" offset)))
  (when (> jump-back #xffff)
    (error (format nil "jump-back is too large: ~a~%" addr)))
  ;; if so, compile it as two bytes
  (let ((high (ash offset -8))
        (low (logand offset #x00ff)))
    (overwrite-chunk chunk high jump-back)
    (overwrite-chunk chunk low (1- jump-back))))

(defun compile-if (chunk form)
  "compile an `if'-conditional form"
  (let ((pred (car form))
        (true-path (cadr form))
        (false-path (cddr form)))
    ;; compile predicate
    (compile-chunk chunk pred)
    ;; jump if predicate is false
    (emit chunk :op-jumpz)
    (write-bogus-offset chunk)
    ;; compile "true" path, then backpatch
    (let* ((offset (get-emesis
                     (compile-chunk chunk true-path)
                     ;; jump past "false" path
                     (emit chunk :op-jump)
                     (write-bogus-offset chunk)))
           (jump-back (+ offset 2)))
      (backpatch-offset chunk offset jump-back))
    ;; compile "false" path, then backpatch
    (let* ((offset (get-emesis (compile-chunk chunk false-path)))
           (jump-back (+ offset 2)))
      (backpatch-offset chunk offset jump-back))))



;; TODO: function creation may need to take in arity
;; as a parameter
(defun compile-lambda (chunk fun)
  "compile a lambda-abstraction thingie"
  (let* ((fun-fun (new-fun))
         (fchunk (chunk-from-fun fun-fun))
         (body (cdr fun))
         (param (car fun))
         (slot (1+ (size *addr-store*))))
    (+depth *addr-store*)
    (store-var *addr-store* param slot)
    (compile-chunk fchunk body)
    (-depth *addr-store*)
    (emit fchunk :op-return)
    (compile-const chunk (make-thing-value fun-fun))))


(defun compile-let (chunk expr)
  "compile an expression inside a let-environment"
  (let #| hehe |# ((bindings (car expr))
                   (body (cdr expr)))
    (+depth *addr-store*)
    (dolist (b bindings) (compile-decl chunk b))
    (compile-chunk chunk body)
    (-depth *addr-store*)))


(defun compile-tuple (chunk elems)
  "compile `elems' a tuple value"
  (debug-log "compiling tuple...")
  (let* ((len (length elems))
         (tuple (make-tuple-value len))
         (index (add-const-compat chunk tuple)))
    ;; push tuple onto stack
    (emit chunk :op-constant)
    (write-chunk- chunk index 0)
    ;; set elems
    (loop for e in elems
          for i from 0 do
            (compile-chunk chunk e)
            (emit chunk :op-tup-set)
            (write-chunk- chunk i 0))))


;; TODO: get line info from parser and pass it through
(defun compile-chunk (chunk expr)
  "compile an expression into a chunk"
  (let ((flavor (car expr))
        (form (cdr expr)))
    (case flavor
      (:apply (compile-apply chunk form))
      ((:number :bool) (compile-const chunk (const->val expr)))
      ((:crunch :str) (compile-str chunk form t))
      (:var-decl (compile-decl chunk form))
      (:sym (compile-variable chunk form))
      (:if (compile-if chunk form))
      (:let (compile-let chunk form))
      (:lambda (compile-lambda chunk form))
      (:tuple (compile-tuple chunk form))
      (otherwise (error (format nil "unknown expr flavor: ~s~%" flavor))))))


(defun fflush ()
  (cffi:foreign-funcall "fflush" :pointer stdout :int))



(defun crocus-eval (chunk code)
  "compile and run crocus code"
  (let ((terms (parse 'forms code)))
    (mapcar (lambda (term) (crocus-eval-term chunk term)) terms))
  (fflush))


(defun crocus-eval-term (chunk term)
  "compile and run a single term"
  (typecheck term)
  (compile-chunk chunk term)
  (emit chunk :op-return)
  (disassemble-chunk chunk "chunk disassembly")
  (fflush)
  (let ((ret (run-chunk chunk)))
    (when (/= 0 ret)
      (error (format nil "crocus error: ~a~%" ret)))
    (fflush)))


(defun read-prompt (prompt)
  "read a line from stdin starting with a prompt"
  (let ((input (cl-readline:readline :prompt prompt)))
    (or input (sb-ext:exit :code 0))))


(defun repl ()
  (let ((toplevel (make-chunk)))
    (loop
      (handler-case (let ((input (read-prompt "% ")))
                      (crocus-eval toplevel input))
        (t (c)
          (format t "error: ~a~%" c)
          (values 0 c))))))

(defun quick-eval (code)
  "quick evaluation, for testing"
  (init-vm)
  (let ((toplevel (make-chunk))
        (ast (parse 'form code)))
    (crocus-eval-term toplevel ast)))

;; output:
;; - stdout from crocus vm (ie `print' output)
;; - top of stack @ end of each chunk
(defun main ()
  "get in the soup"
  (init-vm)
  (repl)
  (uiop:quit))
;; see http://atomized.org/blog/2020/07/06/common-lisp-in-practice/
;; for more on how to make a cli tool in lisp. near the end is a
;; section on how to parse options unix-style.
