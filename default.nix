with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "crocus";
  buildInputs = [ pkgconfig libffi ];
}
